package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

public class SliceManager
	{

		public List<Slice> slices = Collections.synchronizedList(new ArrayList<Slice>());
		public static MainPanel mainPanel;
		public int number = 0;

		SliceManager(MainPanel mainPanel)
			{
				this.mainPanel = mainPanel;
			}

		public int push(String string)
			{
				number++;
				ImageIcon tmpImage = new ImageIcon(string);
				// System.out.println(mainPanel.getWidth());
				slices.add(new Slice(string, mainPanel.getWidth() / 2 - tmpImage.getIconWidth() / 2, mainPanel.getHeight() / 2 - tmpImage.getIconWidth() / 2, tmpImage, number));
				// slices.add(new Slice(100-tmpImage.getIconWidth()/2,
				// 100-tmpImage.getIconWidth()/2, tmpImage, number));

				return number;
			}

		Slice getSlice(int num)
			{
				for (Iterator<Slice> it = slices.iterator(); it.hasNext();)
					{
						Slice slice = it.next();
						if (slice.getNumber() == num)
							{
								return slice;
							}
					}
				return null;
			}

		void removeSlice(Slice tmp)
			{
				synchronized (slices)
					{
						Iterator<Slice> it = slices.iterator();
						while (it.hasNext())
							{
								Slice slice = it.next();
								if (slice.getNumber() == tmp.getNumber())
									{
										it.remove();
									}
							}
					}
				mainPanel.repaint();
			}

		Slice setActiveSlice(MouseEvent e)
			{

				Slice tmpSlice;
				if ((tmpSlice = isSlice(e)) != null)
					{
						for (Iterator<Slice> it = slices.iterator(); it.hasNext();)
							{
								Slice slice = it.next();
								slice.setActive(false);
							}
						getSlice(tmpSlice.getNumber()).setActive(true);
						return tmpSlice;
					}else
						return null;
			}

		Slice isSlice(MouseEvent e)
			{
				for (Iterator<Slice> it = slices.iterator(); it.hasNext();)
					{
						Slice slice = it.next();
						if (slice.isSlice(e))
							{
								return slice;
							}
					}
				return null;
			}

		Slice getActiveSlice()
			{
				for (Iterator<Slice> it = slices.iterator(); it.hasNext();)
					{
						Slice slice = it.next();
						if (slice.isActive() == true)
							{
								return slice;
							}
					}
				return null;
			}

		void draw(Graphics g)
			{
				synchronized (slices)
					{
						for (Iterator<Slice> it = slices.iterator(); it.hasNext();)
							{
								Slice slice = it.next();
								if (!slice.isActive())
									slice.draw(g);
							}
						// Activa slice on top.
						for (Iterator<Slice> it = slices.iterator(); it.hasNext();)
							{
								Slice slice = it.next();
								if (slice.isActive())
									slice.draw(g);
							}
					}
			}

		void clear()
			{
				number = 0;
				slices.clear();
			}

	}

class Slice
	{
		private int number;
		private String name;
		private String className;
		private int x;
		private int y;
		private ImageIcon img;
		private int width;
		private int height;
		private Color color;
		private Rectangle rect;
		private boolean isActive;

		long beforeTime, afterTime, timeDiff, sleepTime;
		boolean changeColor = true;

		static Font f = new Font("Dialog", Font.PLAIN, 9);
		static Map<TextAttribute, Object> map = new Hashtable();

			{
				map.put(TextAttribute.BACKGROUND, Color.blue);
				map.put(TextAttribute.FONT, Font.DIALOG);
				map.put(TextAttribute.SIZE, 9);
			}

		public Slice(String name, int x, int y, ImageIcon img, int number)
			{
				super();
				this.name = name;
				this.className = name.substring(name.lastIndexOf("\\") + 1, name.lastIndexOf("."));// + "_" + number;
				this.number = number;
				this.x = x;
				this.y = y;
				this.img = img;
				this.width = img.getIconWidth();
				this.height = img.getIconHeight();
				this.color = Color.GREEN;
				this.rect = new Rectangle(x, y, width, height);
				this.isActive = false;
				beforeTime = System.nanoTime();
			}

		public String getClassName()
			{
				return className;
			}

		public void setClassName(String className)
			{
				this.className = className;
			}

		public int getWidth()
			{
				return width;
			}

		public void setWidth(int width)
			{
				this.width = width;
			}

		public int getHeight()
			{
				return height;
			}

		public void setHeight(int height)
			{
				this.height = height;
			}

		public Color getColor()
			{
				return color;
			}

		public void setColor(Color color)
			{
				this.color = color;
			}

		public Rectangle getRect()
			{
				return rect;
			}

		public void setRect(Rectangle rect)
			{
				this.rect = rect;
			}

		public boolean isActive()
			{
				return isActive;
			}

		public void setActive(boolean isActive)
			{
				this.isActive = isActive;
			}

		public String getName()
			{
				return name;
			}

		public void setName(String name)
			{
				this.name = name;
			}

		public int getX()
			{
				return this.x;
			}

		public Slice setX(int x)
			{
				this.x = x;
				this.rect.x = x;
				return this;
			}

		public int getY()
			{
				return this.y;
			}

		public Slice setY(int y)
			{
				this.y = y;
				this.rect.y = y;
				return this;
			}
		public Slice setXY(Point xy)
		{
			this.x = xy.x;
			this.y = xy.y;			
			this.rect.x = x;
			this.rect.y = y;			
			return this;
		}
		public ImageIcon getImg()
			{
				return img;
			}

		public void setImg(ImageIcon img)
			{
				this.img = img;
			}

		public int getNumber()
			{
				return number;
			}

		public void setNumber(int number)
			{
				this.number = number;
			}

		void draw(Graphics g)
			{

				img.paintIcon(SliceManager.mainPanel, g, x, y);

				afterTime = System.nanoTime();
				timeDiff = afterTime - beforeTime;

				g.setColor(color);
				f = f.deriveFont(map);
				g.setFont(f);

				// Font f = new Font("Dialog", Font.PLAIN, 9);
				// f.setFont(f);

				if (this.isActive)
					{
						if (timeDiff / 1000 > 250000)
							{
								changeColor = !changeColor;
							}

						if (changeColor)
							{
								g.setColor(Color.red);
							} else
							{
								g.setColor(Color.blue);
							}

						g.drawRect(x, y, width, height);
						SliceManager.mainPanel.repaint();
					} else
					{
						g.drawRect(x, y, width, height);

						g.drawString(String.valueOf(number), x + 1, y + 10);
					}

				if (timeDiff / 1000 > 250000)
					{
						beforeTime = System.nanoTime();
					}
			}

		boolean isSlice(MouseEvent e)
			{
				if (rect.intersects(new Rectangle(e.getX(), e.getY(), 1, 1)))
					{
						return true;
					}
				return false;
			}
	}