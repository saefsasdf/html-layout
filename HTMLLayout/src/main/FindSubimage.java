package main;

import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_32F;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvMinMaxLoc;
import static org.bytedeco.javacpp.opencv_core.cvSize;
import static org.bytedeco.javacpp.opencv_core.cvZero;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvLoadImage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_CCORR_NORMED;
import static org.bytedeco.javacpp.opencv_imgproc.cvMatchTemplate;

import java.awt.Point;
import java.util.Iterator;

import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.opencv_core.CvPoint;
import org.bytedeco.javacpp.opencv_core.IplImage;

public class FindSubimage implements Runnable {

	long startTime;
	long endTime;
	int notFoundCounter = 0;

	public static Point findImages(String image_name, String template_name) {

		System.out.println("image_name = " + image_name);
		System.out.println("template_name = " + template_name);

		CvPoint maxLoc = null;
		try {
			IplImage src = cvLoadImage(image_name, 0);
			IplImage tmp = cvLoadImage(template_name, 0);

			if ((src == null) || (tmp == null)) {
				return null;
			}

			IplImage result = cvCreateImage(
					cvSize(src.width() - tmp.width() + 1,
							src.height() - tmp.height() + 1), IPL_DEPTH_32F,
					src.nChannels());

			cvZero(result);

			// Match Template Function from OpenCV
			cvMatchTemplate(src, tmp, result, CV_TM_CCORR_NORMED);

			// double[] min_val = new double[2];
			// double[] max_val = new double[2];
			DoublePointer min_val = new DoublePointer();
			DoublePointer max_val = new DoublePointer();

			CvPoint minLoc = new CvPoint();
			maxLoc = new CvPoint();

			cvMinMaxLoc(result, min_val, max_val, minLoc, maxLoc, null);
		} catch (Exception e) {
			MainFrame.bottomTextArea.append("\n Error when reading file: "
					+ template_name);
			e.printStackTrace();
		}

		System.out.println("x = " + maxLoc.x());
		System.out.println("y = " + maxLoc.y());

		return new Point(maxLoc.x(), maxLoc.y());
	}

	@Override
	public void run() {
		notFoundCounter = 0;
		startTime = System.currentTimeMillis();
		for (Iterator<Slice> it = MainFrame.panel.sliceManager.slices
				.iterator(); it.hasNext();) {
			Slice slice = it.next();
			Point t = findImages(MainFrame.currentMaketName, slice.getName());
			if (t != null) {
				System.out.println(" x = " + t.x);
				System.out.println(" y = " + t.y);
				MainFrame.bottomTextArea.append("\n x = " + t.x + ", y = "
						+ t.y + " for: " + slice.getName());
				t.x += 50;
				t.y += 50;
				slice.setXY(t);

			} else {
				MainFrame.bottomTextArea.append("\n Can`t find position for: "
						+ slice.getName());
				notFoundCounter++;
			}
		}
		endTime = System.currentTimeMillis();
		MainFrame.bottomTextArea.append("\nauto position end. ("
				+ (endTime - startTime) / 100 + " s, " + notFoundCounter
				+ " images not found)");
		MainFrame.panel.repaint();
		MainFrame.createSaveHTML();
	}
}
