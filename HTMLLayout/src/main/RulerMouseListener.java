package main;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;

import javax.activation.MailcapCommandMap;

public class RulerMouseListener implements MouseListener, MouseMotionListener {
	private Guide tmp = null;

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int orientation = ((Ruler) e.getComponent()).getOrientation();
		if (orientation == Ruler.VERTICAL) {
			tmp = MainPanel.guidesManager.add(orientation, e.getX()
					- MainPanel.rulerWith + 5);

		} else {
			// tmp = MainPanel.guidesManager.add(orientation, e.getY()
			// - MainPanel.rulerWith
			// + MainFrame.js.getVerticalScrollBar().getValue()-50
			// );
 
				tmp = MainPanel.guidesManager.add(orientation, MainFrame.js
						.getVerticalScrollBar().getValue() + 1);

			System.out.println(e.getY());
			System.out.println(MainPanel.rulerWith);
			System.out
					.println(MainFrame.js.getVerticalScrollBar().getValue() - 50);
		}
		MainFrame.panel.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (tmp == null)
			tmp = MainPanel.guidesManager.isForRulerGuide(e);
		if (tmp != null) {
			if (tmp.orientation == Guide.HORIZONTAL) {
				tmp.pos = e.getY() - MainPanel.rulerWith;
				MainFrame.panel.setCursor(Cursor
						.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			} else {
				tmp.pos = e.getX() - MainPanel.rulerWith;
				MainFrame.panel.setCursor(Cursor
						.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			}
		}

		MainFrame.panel.repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {

		tmp = MainPanel.guidesManager.isForRulerGuide(e);
		if (tmp != null) {
			if (tmp.orientation == Guide.HORIZONTAL) {
				MainFrame.panel.setCursor(Cursor
						.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			} else {
				MainFrame.panel.setCursor(Cursor
						.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			}
		}

	}

}
