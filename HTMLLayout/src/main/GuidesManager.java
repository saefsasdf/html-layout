/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author User
 */
class GuidesManager implements Serializable
	{

		int amount = 0;
		List<Guide> guides = Collections.synchronizedList(new ArrayList());

		GuidesManager()
			{
				// System.out.println("draw in constructor = " );
			}

		Guide add(int orientation, int pos)
			{
				amount++;
				Guide tmp = new Guide(orientation, pos, amount);
				guides.add(tmp);
				return tmp;
			}

		void draw(Graphics g)
			{
				if (guides.size() < 1)
					{
						return;
					}
				for (Iterator<Guide> it = guides.iterator(); it.hasNext();)
					{
						Guide guides1 = it.next();
						guides1.draw(g);
					}
			}

		Guide isGuide(MouseEvent e)
			{
				for (Iterator<Guide> it = guides.iterator(); it.hasNext();)
					{
						Guide guides1 = it.next();
						if (((Math.abs(guides1.pos - e.getX()) < 4) && (guides1.orientation == Ruler.VERTICAL))
								|| (Math.abs(guides1.pos - e.getY()) < 4 && (guides1.orientation == Ruler.HORIZONTAL)))
							{
								return guides1;
							}
					}
				return null;
			}

		Guide isForRulerGuide(MouseEvent e)
			{
				for (Iterator<Guide> it = guides.iterator(); it.hasNext();)
					{
						Guide guides1 = it.next();
						if (((Math.abs(guides1.pos - (e.getX() - MainPanel.rulerWith)) < 4) && (guides1.orientation == Ruler.VERTICAL))
								|| (Math.abs(guides1.pos - (e.getY() - MainPanel.rulerWith)) < 4 && (guides1.orientation == Ruler.HORIZONTAL)))
							{
								return guides1;
							}
					}
				return null;
			}

		List<Guide> getNeighboringGuides(MouseEvent e)
			{
				List<Guide> tmp = Collections.synchronizedList(new ArrayList());
				for (Iterator<Guide> it = guides.iterator(); it.hasNext();)
					{
						Guide guides1 = it.next();
						if (((Math.abs(guides1.pos - e.getX()) < 4) && (guides1.orientation == Ruler.VERTICAL))
								|| (Math.abs(guides1.pos - e.getY()) < 4 && (guides1.orientation == Ruler.HORIZONTAL)))
							{
								tmp.add(guides1);
							}
					}
				return tmp;
			}

		void clear()
			{
				guides.clear();
			}
	}

class Guide implements Serializable
	{

		public static final int HORIZONTAL = 1;
		public static final int VERTICAL = 0;
		int orientation;
		int pos;
		int num;

		Guide(int orientation, int pos, int num)
			{
				this.orientation = orientation;
				this.pos = pos;
				this.num = num;
			}

		public void draw(Graphics g)
			{
				//g.setColor(Color.red);
				g.setColor(Color.decode("#a2f6df"));
				if (orientation == HORIZONTAL)
					{
						g.drawLine(0, pos, 2500, pos);
					} else
					{
						g.drawLine(pos, 0, pos, 2000);
					}
			}
	}
