package main;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MainPanel extends JPanel implements MouseListener,
		MouseMotionListener {

	private static final long serialVersionUID = 1L;
	private JFrame mainFrame;
	public static SliceManager sliceManager;
	private ImageIcon img;
	private boolean moveFlag = false;
	private int tx, ty, x, y;

	public static GuidesManager guidesManager = new GuidesManager();
	private Guide tmpGuide;

	public static int startLayoutX = 50, startLayoutY = 50, rulerWith = 13;

	MainPanel(JFrame mainFrame) {
		this.mainFrame = mainFrame;
		sliceManager = new SliceManager(this);
		setBackground(Color.black);
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	@Override
	public void mouseDragged(MouseEvent e) {

		Slice activeSlice = MainPanel.sliceManager.getActiveSlice();

		if (moveFlag) {
			activeSlice.setX(tx + e.getX() - x).setY(ty + e.getY() - y);
		}
		if (tmpGuide != null) {
			if (tmpGuide.orientation == Guide.HORIZONTAL) {
				tmpGuide.pos = e.getY();
			} else {
				tmpGuide.pos = e.getX();
			}
		}
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {

		MainFrame.cursorPosition.setText("x: " + (e.getX() - startLayoutX)
				+ ",y: " + (+e.getY() - startLayoutY));

		Slice activeSlice = MainPanel.sliceManager.getActiveSlice();
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		if (activeSlice != null) {
			if (activeSlice.isSlice(e)) {
				setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
			} else {
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		}
		tmpGuide = guidesManager.isGuide(e);
		if (tmpGuide != null) {
			if (tmpGuide.orientation == Guide.HORIZONTAL) {
				setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			} else {
				setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		getSliceManager().setActiveSlice(e);

		if (e.getComponent() instanceof Ruler) {
			int orientation = ((Ruler) e.getComponent()).getOrientation();
			int offset;
			if (orientation == Ruler.HORIZONTAL) {
				offset = 10 + MainFrame.js.getVerticalScrollBar().getValue();
				if (MainFrame.js.getVerticalScrollBar().getValue() < startLayoutY) {
					offset += startLayoutY;
				}
			} else {
				offset = 10 + MainFrame.js.getHorizontalScrollBar().getValue();
				if (MainFrame.js.getHorizontalScrollBar().getValue() < startLayoutX) {
					offset += startLayoutX;
				}
			}
			guidesManager.add(orientation, offset);
			repaint();
		}
		repaint();
		checkPopup(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stubjh

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		Slice activeSlice = MainPanel.sliceManager.getActiveSlice();
		if (activeSlice != null) {
			if (activeSlice.isSlice(e)) {
				moveFlag = true;
				tx = activeSlice.getX();
				ty = activeSlice.getY();
				x = e.getX();
				y = e.getY();
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		moveFlag = false;
	}

	public SliceManager getSliceManager() {
		return sliceManager;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (img != null) {
			img.paintIcon(this, g, startLayoutX, startLayoutY);
			g.setColor(Color.red);
			g.drawLine(0, startLayoutY, 2500, startLayoutY);
			g.drawLine(startLayoutX, 0, startLayoutX, 2500);
			g.drawLine(img.getIconWidth() + startLayoutX, 0, img.getIconWidth()
					+ startLayoutX, 2500);
			g.drawLine(0, img.getIconHeight() + startLayoutY, 2500,
					img.getIconHeight() + startLayoutY);

		}
		getSliceManager().draw(g);
		guidesManager.draw(g);
	}

	void setImg(String fileName) {
		img = new ImageIcon(fileName);
		repaint();
	}

	ImageIcon getImg() {
		return img;
	}

	private void checkPopup(MouseEvent e) {
		// if (e.isPopupTrigger())
		if (getSliceManager().setActiveSlice(e) != null) {
			repaint();
			if (SwingUtilities.isRightMouseButton(e)) {
				MainFrame.slicePopup.show(this, e.getX(), e.getY());
			}
		}
	}
}
