package main;

public class Css {

	public static String reset = ""
			+ "/* http://meyerweb.com/eric/tools/css/reset/ \r\n"
			+ "   v2.0 | 20110126\r\n" + "   License: none (public domain)\r\n"
			+ "*/\r\n" + "\r\n"
			+ "html, body, div, span, applet, object, iframe,\r\n"
			+ "h1, h2, h3, h4, h5, h6, p, blockquote, pre,\r\n"
			+ "a, abbr, acronym, address, big, cite, code,\r\n"
			+ "del, dfn, em, img, ins, kbd, q, s, samp,\r\n"
			+ "small, strike, strong, sub, sup, tt, var,\r\n"
			+ "b, u, i, center,\r\n" + "dl, dt, dd, ol, ul, li,\r\n"
			+ "fieldset, form, label, legend,\r\n"
			+ "table, caption, tbody, tfoot, thead, tr, th, td,\r\n"
			+ "article, aside, canvas, details, embed, \r\n"
			+ "figure, figcaption, footer, header, hgroup, \r\n"
			+ "menu, nav, output, ruby, section, summary,\r\n"
			+ "time, mark, audio, video {\r\n" + "	margin: 0;\r\n"
			+ "	padding: 0;\r\n" + "	border: 0;\r\n" + "	font-size: 100%;\r\n"
			+ "	font: inherit;\r\n" + "	vertical-align: baseline;\r\n"
			+ "}\r\n" + " \r\n"
			+ "article, aside, details, figcaption, figure, \r\n"
			+ "footer, header, hgroup, menu, nav, section {\r\n"
			+ "	display: block;\r\n" + "}\r\n" + "body {\r\n"
			+ "	line-height: 1;\r\n" + "}\r\n" + "ol, ul {\r\n"
			+ "	list-style: none;\r\n" + "}\r\n" + "blockquote, q {\r\n"
			+ "	quotes: none;\r\n" + "}\r\n"
			+ "blockquote:before, blockquote:after,\r\n"
			+ "q:before, q:after {\r\n" + "	content: '';\r\n"
			+ "	content: none;\r\n" + "}\r\n" + "table {\r\n"
			+ "	border-collapse: collapse;\r\n" + "	border-spacing: 0;\r\n"
			+ "}\n\n";

	public static String wrap = ".wrap {\r\n" + 
			"    position: relative;\r\n" + 
			"    margin: 0 auto;\r\n" + 
			"    height: auto;\r\n" + 
			"    min-height: 100%;\r\n" + 
			"    width: 990px;\r\n" + 
			"    background-color: #fff;\r\n" + 
			"}\n\n";




}
