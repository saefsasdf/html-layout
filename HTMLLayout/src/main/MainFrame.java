package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.text.DefaultCaret;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.Style;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;

public class MainFrame extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	public static Rectangle r;
	private JButton loadMaket;
	private JButton loadSlices;
	private JButton autoPosition;
	private JButton selectAll;
	private JButton clearGuides;
	private JButton saveHTML;
	// private JToggleButton clearGuides, saveHTML;
	public static JLabel cursorPosition;
	public static JLabel tmpInfo;
	public static JTextArea bottomTextArea;
	public static JTabbedPane jtp;

	public static JScrollPane js;
	public static JPanel content;
	public static MainPanel panel;
	public static RSyntaxTextArea textArea = new RSyntaxTextArea(20, 60);
	public static String dirPath;
	public static String currentMaketName;

	public static JCheckBox addCss;
	public static JCheckBox divCenter;
	public static JCheckBox showTemplate;

	// Slice poup
	public static JPopupMenu slicePopup = new JPopupMenu();

	public MainFrame(String title) {
		super(title);

		r = this.getBounds();
		String myJarPath = MainFrame.class.getProtectionDomain()
				.getCodeSource().getLocation().getPath();
		dirPath = new File(myJarPath).getParent();
		dirPath = dirPath.replace("%20", " ");
		dirPath = dirPath.replace("\\", "/");
		// System.out.println("dirPath: " + dirPath);

		File myPath = new File(dirPath + "/HTMLLayout_files/images/");
		// myPath.mkdir();
		myPath.mkdirs();

		File d = new File(dirPath + "/HTMLLayout_files/slices.txt");
		try {
			if (!d.exists()) {
				d.createNewFile();
			}
			d = new File(dirPath + "/HTMLLayout_files/guides.txt");
			if (!d.exists()) {
				d.createNewFile();
			}
			d = new File(dirPath + "/HTMLLayout_files/mail.html");
			if (!d.exists()) {
				d.createNewFile();
			}
		} catch (IOException ex) {
			Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null,
					ex);
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setFocusable(true);

		panel = new MainPanel(this);

		// panel.getSliceManager().push("C:\\Documents and Settings\\User\\git\\HTMLLayout\\HTMLLayout\\images\\competition_img.png");

		jtp = new JTabbedPane();
		content = new JPanel(new BorderLayout());
		loadMaket = new JButton("Load template");
		loadSlices = new JButton("Load slices");
		autoPosition = new JButton("Auto position");
		selectAll = new JButton("Select all");
		clearGuides = new JButton("Clear guides");
		saveHTML = new JButton("Save");
		cursorPosition = new JLabel();
		tmpInfo = new JLabel();

		addCss = new JCheckBox("reset.css", true);
		divCenter = new JCheckBox("div center", true);
		showTemplate = new JCheckBox("show template", true);

		loadMaket.addActionListener(this);
		loadSlices.addActionListener(this);
		autoPosition.addActionListener(this);
		selectAll.addActionListener(this);
		clearGuides.addActionListener(this);
		saveHTML.addActionListener(this);

		addCss.addActionListener(this);
		divCenter.addActionListener(this);
		showTemplate.addActionListener(this);

		JPanel bottom = new JPanel();
		bottom.setLayout(new BorderLayout());
		bottomTextArea = new JTextArea();
		bottomTextArea.setLineWrap(true);
		bottomTextArea.setRows(4);
		bottomTextArea.setWrapStyleWord(true);
		bottomTextArea.setEditable(false);

		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension dim = tk.getScreenSize();

		DefaultCaret caret = (DefaultCaret) bottomTextArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JScrollPane jScrollPane1 = new JScrollPane(bottomTextArea);
		// jScrollPane1.getVerticalScrollBar().addAdjustmentListener(new
		// AdjustmentListener() {
		// public void adjustmentValueChanged(AdjustmentEvent e) {
		// e.getAdjustable().setValue(e.getAdjustable().getMaximum());
		// }
		// });
		bottom.add(jScrollPane1, BorderLayout.CENTER);
		content.add(bottom, BorderLayout.PAGE_END);

		JPanel leftSubPanel = new JPanel();
		leftSubPanel.setLayout(new GridLayout(9, 1));

		// leftSubPanel.add(createLabel(" "));
		leftSubPanel.add(loadMaket);
		leftSubPanel.add(loadSlices);
		leftSubPanel.add(autoPosition);
		leftSubPanel.add(selectAll);
		leftSubPanel.add(clearGuides);
		leftSubPanel.add(saveHTML);

		leftSubPanel.add(addCss);
		leftSubPanel.add(divCenter);
		leftSubPanel.add(showTemplate);

		leftSubPanel.setMaximumSize(new Dimension(100, 700));
		// leftSubPanel.setBorder(BorderFactory.createLineBorder(Color.gray));

		Border border = BorderFactory.createLineBorder(Color.gray);
		Border margin = new EmptyBorder(4, 4, 4, 4);
		leftSubPanel.setBorder(new CompoundBorder(border, margin));

		JPanel leftPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weighty = 1;

		leftPanel.add(leftSubPanel, gbc);
		leftPanel.setMaximumSize(new Dimension(120, 700));
		// leftPanel.setBorder(BorderFactory.createLineBorder(Color.green));
		leftPanel.setBorder(new EmptyBorder(19, 1, 1, 1));

		content.add(leftPanel, BorderLayout.LINE_START);

		JPanel rightPanel = new JPanel();
		JPanel tmpRightPanel = new JPanel();
		tmpRightPanel.setLayout(new BoxLayout(tmpRightPanel, BoxLayout.Y_AXIS));
		tmpRightPanel.add(createLabel("Right"));
		tmpRightPanel.add(cursorPosition);
		tmpRightPanel.add(tmpInfo);
		rightPanel.add(tmpRightPanel);
		content.add(rightPanel, BorderLayout.LINE_END);

		js = new JScrollPane(panel);

		Ruler rowView = new Ruler(MainPanel.startLayoutX,
				MainPanel.startLayoutX, MainPanel.rulerWith, Ruler.HORIZONTAL);
		rowView.setPreferredWidth(2000);
		rowView.addMouseListener(new RulerMouseListener());
		rowView.addMouseMotionListener(new RulerMouseListener());
		js.setColumnHeaderView(rowView);

		Ruler columnView = new Ruler(MainPanel.startLayoutX,
				MainPanel.startLayoutY, MainPanel.rulerWith, Ruler.VERTICAL);
		columnView.setPreferredHeight(3500);
		columnView.addMouseListener(new RulerMouseListener());
		columnView.addMouseMotionListener(new RulerMouseListener());
		js.setRowHeaderView(columnView);
		js.getVerticalScrollBar().addAdjustmentListener(
				new AdjustmentListener() {
					@Override
					public void adjustmentValueChanged(AdjustmentEvent evt) {
						// System.out.println("value HB = " + evt.getValue());
						tmpInfo.setText(String.valueOf(js
								.getVerticalScrollBar().getValue() - 50));
					}
				});

		MouseWheelController mwc = new MouseWheelController(js, 20);
		mwc.install();

		content.add((js), BorderLayout.CENTER);

		// Image Editor
		jtp.addTab(" Image editor ", content);

		// HTML Editor
		JPanel htmlEditorPane = new JPanel(new GridLayout());
		textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
		textArea.setCodeFoldingEnabled(true);
		textArea.setAntiAliasingEnabled(true);
		textArea.setBackground(Color.BLACK);

		// Not black font in css text.
		textArea.getSyntaxScheme().setStyle(20, new Style(Color.gray));

		JPopupMenu popup = textArea.getPopupMenu();
		popup.addSeparator();
		htmlEditorPane.add(new RTextScrollPane(textArea));

		jtp.addTab("HTML Editor", htmlEditorPane);

		javax.swing.event.ChangeListener changeListener = new ChangeListenerImpl(
				this);
		jtp.addChangeListener(changeListener);
		getContentPane().add(jtp);

		// Poup Slice menu
		ActionListener menuListener = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String menuItem = event.getActionCommand();
				switch (menuItem) {
				case "Delete":
					panel.sliceManager.removeSlice(panel.sliceManager
							.getActiveSlice());
					break;
				case "Image + ref":
					// panel.sliceManager.getActiveSlice().setType(
					// SliceType.IMAGE_REF);
					// bottomTextArea.append("\nImage + ref");
					break;
				case "Text":
					// panel.sliceManager.getActiveSlice().setType(SliceType.TEXT);
					// panel.sliceManager.getActiveSlice().setUserText(
					// infoSlice.getText());
					break;
				case "OCR text":
					// panel.sliceManager.getActiveSlice().setType(
					// SliceType.OCR_TEXT);
					break;
				default:
					break;
				}
			}
		};
		JMenuItem item;
		slicePopup.add(item = new JMenuItem("Delete"));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.addActionListener(menuListener);
		slicePopup.add(item = new JMenuItem("Image + ref"));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.addActionListener(menuListener);
		slicePopup.add(item = new JMenuItem("Text"));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.addActionListener(menuListener);
		slicePopup.add(item = new JMenuItem("OCR text"));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.addActionListener(menuListener);
		slicePopup.add(item = new JMenuItem("Full"));
		item.setHorizontalTextPosition(JMenuItem.RIGHT);
		item.addActionListener(menuListener);
		slicePopup.addSeparator();
		slicePopup.add(item = new JMenuItem("Settings . . ."));
		item.addActionListener(menuListener);

		slicePopup.setLabel("Justification");
		slicePopup.setBorder(new BevelBorder(BevelBorder.RAISED));

		setSize(dim.width, dim.height);// - 300);// -25
		// setSize(1200, 800);

		setVisible(true);
		bottomTextArea.append("HTML Layout Editor");
		bottomTextArea.append("\ndirPath: " + dirPath.replace("/", "\\"));

	}

	public Dimension getFrameRect() {
		return this.getContentPane().getSize();
	}

	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		if (s == clearGuides) {
			panel.guidesManager.clear();
			panel.repaint();
		}
		if ((s == loadMaket) || (s == loadSlices)) {
			JFileChooser ch = new JFileChooser(new File("."));
			ch.setMultiSelectionEnabled(true);
			int opt = ch.showOpenDialog(null);
			if (opt == JFileChooser.APPROVE_OPTION) {
				if (s == loadMaket) {
					currentMaketName = ch.getSelectedFile().toString();
					ImageIcon currentImage = new ImageIcon(ch.getSelectedFile()
							.toString());
					panel.setPreferredSize(new Dimension(currentImage
							.getIconWidth() + 100,
							currentImage.getIconHeight() + 100));
					js.setPreferredSize(new Dimension(currentImage
							.getIconWidth() + 100,
							currentImage.getIconHeight() + 100));
					panel.setImg(ch.getSelectedFile().toString());
					String loadedImageName = ch.getSelectedFile().toString();
					bottomTextArea.append("\nlayout: " + loadedImageName);
				} else {
					// System.out.println(ch.getSelectedFile().toString());
					File[] loadedImageName = ch.getSelectedFiles();
					for (File file : loadedImageName) {
						panel.getSliceManager().push(file.toString());
						bottomTextArea.append("\nslice: " + file.toString());
					}

				}
				// Put new maket in FF right panel
				// picLabel = new JLabel(new
				// ImageIcon(loadedImageName));
				// panelRight.removeAll();
				// panelRight.add(picLabel);

				SwingUtilities.updateComponentTreeUI(panel);
				panel.repaint();
			}
		}
		if (s == autoPosition) {
			bottomTextArea.append("\nauto position start...");
			FindSubimage fs = new FindSubimage();
			Thread t = new Thread(fs);
			t.start();
		}
		if (s == selectAll) {
			for (Iterator<Slice> it = panel.sliceManager.slices.iterator(); it
					.hasNext();) {
				Slice slice = it.next();
				slice.setActive(true);
			}
			panel.repaint();
		}
		if ((s == saveHTML) || (s == addCss) || (s == divCenter)
				|| (s == showTemplate)) {
			createSaveHTML();
		}
	}

	private JLabel createLabel(String caption)

	{
		JLabel lbl = new JLabel(caption);
		lbl.setPreferredSize(new Dimension(100, 30));
		lbl.setHorizontalAlignment(JLabel.CENTER);
		// lbl.setBorder(BorderFactory.createLineBorder(Color.green,
		// 1));
		return lbl;

	}

	public static void createSaveHTML() {
		MainFrame.loadTextToHTMLEditor();
		String tmpTreeStr = textArea.getText();
		// Source source = new Source(tmpTreeStr);
		// tmpTreeStr = new
		// SourceFormatter(source).setIndentString("   ").setTidyTags(true).setCollapseWhiteSpace(true).toString();

		String fileName = MainFrame.dirPath + "/HTMLLayout_files/mail.html";
		PrintWriter zzz = null;
		try {
			zzz = new PrintWriter(new FileOutputStream(fileName));
		} catch (FileNotFoundException e) {
			bottomTextArea.append("\n������ �������� �����: " + fileName);
		}
		zzz.print(tmpTreeStr);
		zzz.close();
	}

	public static void loadTextToHTMLEditor() {
		String t = "<!DOCTYPE HTML>\n"
				+ "<html>\n"
				+ "  <head>\n"
				+ "    <title> </title>\n"
				+ "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
				+ "    <meta http-equiv=\"Content-Language\" content=\"ru\">\n"
				+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" >\n"
				+ "    <style>\n";

		if (addCss.isSelected())
			t += Css.reset;
		if (divCenter.isSelected())
			t += Css.wrap;

		t += "      img{" + "position: absolute;" + "}\n";
		t += "\n";
		for (Iterator<Slice> it = panel.sliceManager.slices.iterator(); it
				.hasNext();) {
			Slice slice = it.next();

			t += "      ." + slice.getClassName() + "\n";
			t += "      {\n";
			t += "      left: " + (slice.getX() - panel.startLayoutX)
					+ "px; \n";
			t += "      top: " + (slice.getY() - panel.startLayoutY) + "px; \n";
			t += "      width: " + slice.getWidth() + "px; \n";
			t += "      height: " + slice.getHeight() + "px; \n";

			t += "      }\n";
		}
		t += "    </style>\n" + "  </head>\n" + "<body>\n\n";
		t += "    <div class='wrap'>\n";
		if (showTemplate.isSelected())
			t += "<img src=\"file:///" + currentMaketName + "\" /> \n";
		for (Iterator<Slice> it = panel.sliceManager.slices.iterator(); it
				.hasNext();) {
			Slice slice = it.next();
			// t += "\n";
			t += "<img src=\"file:///" + slice.getName() + "\" class=\""
					+ slice.getClassName() + "\" /> \n";
			// t += "\n";

		}
		t += "    </div>\n";
		t += "\n</body>\n" + "</html>\n";

		// Source source = new Source(t);
		// t = new
		// SourceFormatter(source).setIndentString("   ").setTidyTags(true).setCollapseWhiteSpace(true).toString();

		textArea.setText(t);

	}

	public static JScrollPane getJs() {
		return js;
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(new SyntheticaBlackEyeLookAndFeel());
		} catch (Exception e) {

			e.printStackTrace();
		}
		new MainFrame("HTML Layout Editor");
	}

	private class ChangeListenerImpl implements
			javax.swing.event.ChangeListener {

		private MainFrame mainFrame;

		public ChangeListenerImpl() {
		}

		private ChangeListenerImpl(MainFrame mainFrame) {
			this.mainFrame = mainFrame;
		}

		public void stateChanged(ChangeEvent changeEvent) {
			JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent
					.getSource();
			int index = sourceTabbedPane.getSelectedIndex();
			// System.out.println("Tab changed to: "
			// + sourceTabbedPane.getTitleAt(index) + ", �: " + index);
			if (index == 2) { // Browser
				// MainFrame.loadHTMLToBrowser();
			}
			if (index == 1) { // HTML Editor
				createSaveHTML();

			}
		}
	}
}
