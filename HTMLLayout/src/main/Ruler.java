package main;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.*;

public class Ruler extends JComponent {

    public static int HORIZONTAL = 1;
    public static int VERTICAL = 0;
    int startLayoutX, startLayoutY;
    private int rulerWith, orientation;

    public Ruler(int startLayoutX, int startLayoutY, int rulerWith, int orientation) {
        this.startLayoutX = startLayoutX;
        this.startLayoutY = startLayoutY;
        this.rulerWith = rulerWith;
        this.orientation = orientation;
    }

    public void setPreferredHeight(int ph) {
        setPreferredSize(new Dimension(getRulerWith(), ph));
    }

    public void setPreferredWidth(int pw) {
        setPreferredSize(new Dimension(pw, getRulerWith()));
    }

    protected void paintComponent(Graphics g) {

        //g.setColor(Color.white);
        g.setColor(Color.decode("#1c1c1c")); 
        
        g.fillRect(startLayoutX, 0, this.getPreferredSize().width, getRulerWith());
       // g.setColor(Color.black); 
        g.setColor(Color.decode("#5c5c5c"));        

        Font f = new Font("Dialog", Font.PLAIN, 9);
        g.setFont(f);

        g.drawLine(0, getRulerWith()-1, 6500, getRulerWith()-1);
        for (int i = 0; i < 130; i++) {
            g.drawLine(startLayoutX + i * 50, 0, startLayoutX + i * 50, 0 + getRulerWith());
            g.drawLine(startLayoutX + i * 10 + 5, 0 + getRulerWith(), startLayoutX + i * 10 + 5, 0 + getRulerWith() - (getRulerWith() / 3) + 1);
            g.drawLine(startLayoutX + i * 10, 0 + getRulerWith(), startLayoutX + i * 10, 0 + getRulerWith() - (getRulerWith() / 3) * 2 + 3);
            g.drawString(String.valueOf(i * 50), startLayoutX + i * 50 + 2, 0 + 7);
        }

        //g.setColor(Color.white);
        g.setColor(Color.decode("#1c1c1c")); 
        
        g.fillRect(0, startLayoutY, getRulerWith(), 1500);
        //g.setColor(Color.black);
        g.setColor(Color.decode("#5c5c5c"));
        
        f = new Font("Dialog", Font.PLAIN, 9);
        g.setFont(f);

        g.drawLine(getRulerWith()-1, 0, getRulerWith()-1, 6500);
        for (int i = 0; i < 200; i++) {
            g.drawLine(0, startLayoutY + i * 50, 0 + getRulerWith(), startLayoutY + i * 50);
            g.drawLine(0 + getRulerWith(), startLayoutY + i * 10 + 5, 0 + getRulerWith() - (getRulerWith() / 3) + 1, startLayoutY + i * 10 + 5);
            g.drawLine(0 + getRulerWith(), startLayoutY + i * 10, 0 + getRulerWith() - (getRulerWith() / 3) * 2 + 3, startLayoutY + i * 10);

            for (int j = 0; j < (String.valueOf(i * 50)).length(); j++) {
                g.drawString((String.valueOf(i * 50)).substring(j, j + 1), 0 + 2, startLayoutY + i * 50 + 10 + j * 8);
            }
        }
    }

    /**
     * @return the rulerWith
     */
    public int getRulerWith() {
        return rulerWith;
    }
  
     /**
     * @return the orientation
     */
    public int getOrientation() {
        return orientation;
    }
}
